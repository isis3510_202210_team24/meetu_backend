const functions = require("firebase-functions");

const admin = require("firebase-admin");
admin.initializeApp();

const db = admin.firestore();

const fruitTrigger = functions.firestore.document("profiles/{id}");
const likesTrigger = functions.firestore.document("likes/{id}");
const preferencesTrigger = functions.firestore.document("userPreferences/{id}");

async function checkMatches(uidList) {
  try {
    //for each user in the list
    for (let i = 0; i < uidList.length; i++) {
      //checks for a document in the likes colecction
      const likes = await db
        .collection(`likes`)
        .doc(`${uidList[i].userLikedId}`);
      //if there is a document in the likes collection with the user liked id
      if (likes.exists) {
        //get the likes collection
        const likesCollection = await likes.get();
        //get the likes collection data
        const likesCollectionData = likesCollection.data();
        //check the usersLikedId in the likes collection data
        for (let j = 0; j < likesCollectionData.usersLiked.length; j++) {
          //if the user liked id is in the list of users liked
          if (
            likesCollectionData.usersLiked[j].userLikedId ===
            uidList[i].userLikedId
          ) {
            // print the user liked id
            console.log(likesCollectionData.usersLiked[j].userLikedId);
            // create a record in the matches collection containing the user liked id
            await db
              .collection(`matches`)
              .doc(`${likesCollectionData.usersLiked[j]}`)
              .set({
                userLikedId: uidList[i].userLikedId,
              });
            await db
              .collection(`matches`)
              .doc(`${uidList[i].userLikedId}`)
              .set({
                userLikedId: likesCollectionData.usersLiked[j].userLikedId,
              });
          }
        }
      }
    }
  } catch (error) {
    if (error?.code === "6" || error?.code === 6) {
      functions.logger.debug("Duplicated event trigger!");
    } else {
      throw error;
    }
  }
}

exports.likesModidied = likesTrigger.onUpdate(async (change, context) => {
  const newValue = change.after.data();
  var a = newValue.usersLiked;
  console.log(checkMatches(a));
});

async function updateCount(eventId, delta) {
  try {
    // create will throw ALREADY_EXISTS if the event has already been processed
    await db
      .doc(`counter/counterResults/events/${eventId}`)
      .create({ createdAt: admin.firestore.FieldValue.serverTimestamp() });

    await db
      .doc("counter/counterResults")
      .set(
        { count: admin.firestore.FieldValue.increment(delta) },
        { merge: true }
      );

    if (Math.random() < 1.0 / 300) {
      await cleanup();
    }
  } catch (error) {
    if (error?.code === "6" || error?.code === 6) {
      functions.logger.debug("Duplicated event trigger!");
    } else {
      throw error;
    }
  }
}

async function cleanup() {
  const limitDate = new Date(Date.now() - 1000 * 60 * 10);

  const batch = db.batch();

  const pastEvents = await db
    .collection("counter/counterResults/events")
    .orderBy("createdAt", "asc")
    .where("createdAt", "<", limitDate)
    .limit(400)
    .get();

  pastEvents.forEach((event) => batch.delete(event.ref));

  await batch.commit();
}

exports.onCreate = fruitTrigger.onCreate((doc, context) =>
  updateCount(context.eventId, +1)
);

exports.onDelete = fruitTrigger.onDelete((doc, context) =>
  updateCount(context.eventId, -1)
);

// ML FUNCTIONS

const convertToBetweenCeroAndOne = (x) => {
  return Math.log2(Math.abs(x / (1 - x)));
};

const processOneUserPreference = async (profile, recommededByUsers) => {
  console.log("0", profile.data());

  const [sp, prs] = await Promise.all([
    db.collection("recommended").doc(profile.id).get(),
    await db.collection("userPreferences").doc(profile.id).get(),
  ]);

  const prefSnapshot = prs?.data()?.preferences ?? [];

  if (!prefSnapshot.length) {
    return;
  }

  const preferences =
    (
      await db
        .collection("preferences")
        .where(admin.firestore.FieldPath.documentId(), "in", prefSnapshot)
        .get()
    )?.docs?.map((d) => d.data()) ?? [];

  let myscore = preferences
    .map((preference) => preference.name.length + preference.category.length)
    .reduce((a, b) => a + b, 0);

  myscore === 0 ? (myscore = Math.random() * 10) : myscore;
  console.log("2");
  const totalProfiles2 = (
    await db.collection("counter").doc("counterResults").get()
  ).data().count;

  let count2 = totalProfiles2;
  let hadAlreadyCreatedInstance = false;
  while (count2 > 0) {
    const batch = db.batch();
    const profiles2 = await db
      .collection("profiles")
      .orderBy(admin.firestore.FieldPath.documentId())
      .where(admin.firestore.FieldPath.documentId(), "!=", profile.id)
      .offset(totalProfiles2 - count2)
      .limit(100)
      .get();

    for (const it of profiles2.docs) {
      if (profile.id === it?.id) {
        continue;
      }

      const prefSnapshot2 =
        (await db.collection("userPreferences").doc(profile.id).get())?.data()
          ?.preferences ?? [];

      if (!prefSnapshot2.length) {
        continue;
      }

      const preferences2 =
        (
          await db
            .collection("preferences")
            .where(admin.firestore.FieldPath.documentId(), "in", prefSnapshot2)
            .get()
        )?.docs?.map((d) => d.data()) ?? [];

      let score = preferences2
        .map(
          (preference) => preference.name.length + preference.category.length
        )
        .reduce((a, b) => a + b, 0);
      score === 0 ? (score = Math.random() * 10) : score;
      let ponderated = Math.abs(
        convertToBetweenCeroAndOne(score) - convertToBetweenCeroAndOne(myscore)
      );
      ponderated = ponderated === 0 ? Math.random() * 1.1 : ponderated;
      console.table([ponderated]);
      if (ponderated >= 0.5) {
        const myRecomendations = db.collection("recommended").doc(profile.id);
        const recs = await myRecomendations.get();
        if (hadAlreadyCreatedInstance || recs.exists) {
          batch.update(myRecomendations, {
            profiles: [...new Set(recs.data()?.profiles ?? []), it.id],
          });
          recommededByUsers[profile.id] = [
            ...(recommededByUsers[profile.id] ?? []),
            it.id,
          ];
          console.log("3", "updating");
        } else if (!hadAlreadyCreatedInstance) {
          recommededByUsers[profile.id] = [
            ...(recommededByUsers[profile.id] ?? []),
            it.id,
          ];
          batch.create(myRecomendations, { profiles: [it.id] });
          hadAlreadyCreatedInstance = true;
          console.log("3", "crating");
        }
      }
    }
    count2 -= 100;
    await batch.commit();
  }
};

const processPreferences = async () => {
  const db = admin.firestore();

  const recommededByUsers = {};

  const totalProfiles = (
    await db.collection("counter").doc("counterResults").get()
  ).data()?.count;

  let count = totalProfiles;
  while (count > 0) {
    const profiles = await db
      .collection("profiles")
      .orderBy(admin.firestore.FieldPath.documentId())
      .offset(totalProfiles - count)
      .limit(100)
      .get();

    for (let i = 0; i < profiles.size; i++) {
      const profile = profiles.docs[i];
      await processOneUserPreference(profile, recommededByUsers);
    }
    count -= 100;
  }
  console.log(recommededByUsers);
  return recommededByUsers;
};

exports.launchModelAI = functions
  .runWith({
    // Ensure the function has enough memory and time
    // to process large files
    timeoutSeconds: 500,
    memory: "1GB",
  })
  .https.onRequest(async (req, res) => {
    return res.send({ data: await processPreferences() });
  });

exports.onCreatePreferences = preferencesTrigger.onCreate(
  async (doc, context) => {
    const profile = await db.collection("profiles").doc(doc.id).get();
    await processOneUserPreference(profile, {});
  }
);

exports.scheduledFunctionCrontab = functions
  .runWith({
    timeoutSeconds: 500,
    memory: "1GB",
  })
  .pubsub.schedule("5 11 * * *")
  .timeZone("America/Bogota") // Users can choose timezone - default is America/Los_Angeles
  .onRun(async (context) => {
    console.log("This will be run every day at 11:05 AM Eastern!");

    return await processPreferences();
  });

// exports.newUserSingUp = functions.auth.user().onCreate(async (user) => {
//   return db.collection("users").doc(user.uid).set({ email: user.email });
// });

exports.userDeleted = functions.auth.user().onDelete(async (user) => {
  const doc = db.collection("users").doc(user.uid);
  return doc.delete();
});

exports.deleteLike = functions.https.onCall(async ({ myId, deleteId }) => {
  const data = await db
    .collection("likes")
    .doc(myId)
    .get()
    .then((doc) => {
      return doc.data();
    });
  console.log(myId, deleteId);
  console.log(data);
  const newLikes = data.usersLiked.filter(
    ({ userLikedId }) => userLikedId !== deleteId
  );
  console.log(newLikes);

  return db.collection("likes").doc(myId).update({ usersLiked: newLikes });
});

exports.processRecommended = functions
  .runWith({
    timeoutSeconds: 300,
    memory: "1GB",
  })
  .https.onRequest(async (req, res) => {
    const data = await db.collection("profiles").doc(req.query.id).get();

    return res.send({ data: await processOneUserPreference(data, {}) });
  });
